package org.wikimedia.mediawiki.flink.connectors.http.table;

import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.table.connector.format.DecodingFormat;
import org.apache.flink.table.connector.source.AsyncTableFunctionProvider;
import org.apache.flink.table.connector.source.DynamicTableSource;
import org.apache.flink.table.connector.source.LookupTableSource;
import org.apache.flink.table.data.RowData;
import org.apache.flink.table.types.DataType;

import java.util.List;

public class HttpTableSource implements LookupTableSource {
    private final HttpTableSourceOptions options;
    private final List<String> columnNames;
    private final DecodingFormat<DeserializationSchema<RowData>> decodingFormat;
    private final DataType pyhisicalRowDataType;

    public HttpTableSource(HttpTableSourceOptions options,
                           List<String> columnNames,
                           DecodingFormat<DeserializationSchema<RowData>> decodingFormat,
                           DataType pyhisicalRowDataType) {
        this.options = options;
        this.columnNames = columnNames;
        this.decodingFormat = decodingFormat;
        this.pyhisicalRowDataType = pyhisicalRowDataType;
    }

    @Override
    public DynamicTableSource copy() {
        return new HttpTableSource(options, columnNames, decodingFormat, pyhisicalRowDataType);
    }

    @Override
    public String asSummaryString() {
        return "Http table source";
    }

    @Override
    public LookupRuntimeProvider getLookupRuntimeProvider(LookupContext lookupContext) {
        DeserializationSchema<RowData> schemaDecoder = decodingFormat.createRuntimeDecoder(lookupContext, pyhisicalRowDataType);
        return AsyncTableFunctionProvider.of(new HttpAsyncTableFunction(options, columnNames, schemaDecoder));
    }
}
