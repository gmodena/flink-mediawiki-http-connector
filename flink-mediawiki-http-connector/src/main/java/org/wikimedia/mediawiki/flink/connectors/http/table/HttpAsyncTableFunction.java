package org.wikimedia.mediawiki.flink.connectors.http.table;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.table.data.RowData;
import org.apache.flink.table.functions.AsyncTableFunction;
import org.apache.flink.table.functions.FunctionContext;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Result;
import org.eclipse.jetty.client.util.BufferingResponseListener;
import org.wikimedia.mediawiki.flink.connectors.http.util.HttpClientUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
public class HttpAsyncTableFunction extends AsyncTableFunction<RowData> {
    private final HttpTableSourceOptions options;
    private final List<String> columnNames;
    private final String domainColumn;
    private final String responseColumn;
    private final DeserializationSchema<RowData> schemaDecoder;
    private HttpClient httpClient;
    private AtomicInteger httpCallCounter;

    public HttpAsyncTableFunction(
            HttpTableSourceOptions options,
            List<String> columnNames,
            DeserializationSchema<RowData> schemaDecoder
    ) {
        this.options = options;
        this.columnNames = new ArrayList<>(columnNames);
        this.schemaDecoder = schemaDecoder;
        // magic fields
        this.domainColumn = options.getDomainField();
        this.responseColumn = options.getResponseField();
    }

    @Override
    public void open(FunctionContext context) throws Exception {
        super.open(context);

        this.httpCallCounter = new AtomicInteger(0);
        httpClient = HttpClientUtils.buildHttpClient(options.getHttpProxyHost(), options.getHttpProxyPort());

        context
                .getMetricGroup()
                .gauge("mediawiki-http-table-call-counter", () -> httpCallCounter.intValue());
    }

    private Map<String, String> filterLookupParams(Map<String, String> params) {
        return params.entrySet()
                .stream()
                .filter(entry -> !entry.getKey().equals(domainColumn)).collect(Collectors.toMap(Map.Entry::getKey,
                        Map.Entry::getValue));
    }

    // An experiment with json schema decoders.
    private RowData decodeRowData(Map<?, ?> map) throws IOException {
        String json = new ObjectMapper().writeValueAsString(map);
        return schemaDecoder.deserialize(json.getBytes(StandardCharsets.UTF_8));
    }

    private Request buildHttpRequest(Map<String, String> params) {
        String url = HttpClientUtils.buildQueryString(options.getUrl(), filterLookupParams(params));

        httpCallCounter.incrementAndGet();
        return httpClient.newRequest(url).header("user-agent", options.getUserAgent());
    }

    private void process(CompletableFuture<Collection<RowData>> completableFuture, Object... keys) {
        LinkedHashMap<String, String> rowHash = new LinkedHashMap<String, String>();
        Streams.forEachPair(columnNames.stream(), Arrays.stream(keys).map(Object::toString), rowHash::put);

        Request request = buildHttpRequest(rowHash);

        httpCallCounter.incrementAndGet();
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                String content = "";
                if (result.getResponse().getStatus() == 200) {
                    content = getContentAsString();
                } else {
                    log.error("Invalid response from " + result.getResponse().getReason());
                }
                rowHash.put(responseColumn, content);
                try {
                    completableFuture.complete(Collections.singleton(decodeRowData(rowHash)));

                } catch (IOException e) {
                    completableFuture.completeExceptionally(e);
                }
            }
        });
    }

    public void eval(CompletableFuture<Collection<RowData>> result, Object... keys) {
        process(result, keys);
    }
}
