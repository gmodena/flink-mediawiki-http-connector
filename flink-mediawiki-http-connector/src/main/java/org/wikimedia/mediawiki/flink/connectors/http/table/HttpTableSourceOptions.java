package org.wikimedia.mediawiki.flink.connectors.http.table;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nullable;
import java.io.Serializable;

@Data
@SuperBuilder(toBuilder = true)
public class HttpTableSourceOptions implements Serializable {
    private static final long serialVersionUID = 1L;

    private final String url;
    @Nullable
    private final String domainField;
    @Nullable
    private final String responseField;
    @Nullable
    private final String userAgent;
    @Nullable
    private final String httpProxyHost;
    @Nullable
    private final Integer httpProxyPort;
}
