package org.wikimedia.mediawiki.flink.connectors.http.table;

import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.configuration.ConfigOptions;
import org.apache.flink.table.catalog.Column;
import org.apache.flink.table.catalog.ResolvedSchema;
import org.apache.flink.table.connector.format.DecodingFormat;
import org.apache.flink.table.connector.source.DynamicTableSource;
import org.apache.flink.table.data.RowData;
import org.apache.flink.table.factories.DeserializationFormatFactory;
import org.apache.flink.table.factories.DynamicTableSourceFactory;
import org.apache.flink.table.factories.FactoryUtil;
import org.apache.flink.table.types.DataType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class HttpAsyncTableSourceFactory implements DynamicTableSourceFactory {
    public static final String FACTORY_IDENTIFIER = "mediawiki-http";

    public static final ConfigOption<String> URL = ConfigOptions.key("url")
            .stringType()
            .noDefaultValue()
            .withDescription("The url to query. It can contain defaults query string params.");
    public static final ConfigOption<String> DOMAIN_FIELD = ConfigOptions.key("fields.domain")
            .stringType()
            .noDefaultValue()
            .withDescription("Field that stores the domain host.");
    public static final ConfigOption<String> RESPONSE_FIELD = ConfigOptions.key("fields.response")
            .stringType()
            .defaultValue("response")
            .withDescription("Field that stores the response content");
    public static final ConfigOption<String> USER_AGENT = ConfigOptions.key("http.client.header.user-agent")
            .stringType()
            .defaultValue("wmf-mediawiki-stream-enrichment/1.0-SNAPSHOT bot")
            .withDescription("The http client user-agent header.");
    public static final ConfigOption<String> HTTP_PROXY_HOST = ConfigOptions.key("http.client.proxy.host")
            .stringType()
            .noDefaultValue()
            .withDescription("http proxy host.");
    public static final ConfigOption<Integer> HTTP_PROXY_PORT = ConfigOptions.key("http.client.proxy.port")
            .intType()
            .noDefaultValue()
            .withDescription("http proxy port.");

    @Override
    public DynamicTableSource createDynamicTableSource(Context context) {
        FactoryUtil.TableFactoryHelper factoryHelper =
                FactoryUtil.createTableFactoryHelper(this, context);
        factoryHelper.validate();

        // this is the stuff we put in SQL DDL.
        final HttpTableSourceOptions options = HttpTableSourceOptions.builder()
                .url(factoryHelper.getOptions().get(URL))
                .domainField(factoryHelper.getOptions().get(DOMAIN_FIELD))
                .responseField(factoryHelper.getOptions().get(RESPONSE_FIELD))
                .userAgent(factoryHelper.getOptions().get(USER_AGENT))
                .httpProxyHost(factoryHelper.getOptions().get(HTTP_PROXY_HOST))
                .httpProxyPort(factoryHelper.getOptions().get(HTTP_PROXY_PORT))
                .build();

        final List<String> columnNames = context.getCatalogTable().getResolvedSchema().getColumns().stream()
                .filter(Column::isPhysical)
                .map(Column::getName)
                .collect(Collectors.toList());

        DecodingFormat<DeserializationSchema<RowData>> decodingFormat = factoryHelper
                .discoverDecodingFormat(DeserializationFormatFactory.class, FactoryUtil.FORMAT);
        ResolvedSchema resolvedSchema = context.getCatalogTable().getResolvedSchema();
        DataType physicalRowDataType = resolvedSchema.toPhysicalRowDataType();
        return new HttpTableSource(options, columnNames, decodingFormat, physicalRowDataType);
    }

    @Override
    public String factoryIdentifier() {
        return FACTORY_IDENTIFIER;
    }

    @Override
    public Set<ConfigOption<?>> requiredOptions() {
        final Set<ConfigOption<?>> options = new HashSet<>();
        options.add(URL);
        options.add(FactoryUtil.FORMAT);
        return options;
    }

    @Override
    public Set<ConfigOption<?>> optionalOptions() {
        final Set<ConfigOption<?>> options = new HashSet<>();
        options.add(DOMAIN_FIELD);
        options.add(RESPONSE_FIELD);
        options.add(USER_AGENT);
        options.add(HTTP_PROXY_HOST);
        options.add(HTTP_PROXY_PORT);
        return options;
    }
}
