package org.wikimedia.mediawiki.flink.connectors.http;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.catalog.Column;
import org.apache.flink.table.catalog.ResolvedSchema;
import org.apache.flink.table.catalog.UniqueConstraint;
import org.apache.flink.table.connector.source.AsyncTableFunctionProvider;
import org.apache.flink.table.data.RowData;
import org.apache.flink.table.runtime.connector.source.LookupRuntimeProviderContext;
import org.junit.jupiter.api.Test;
import org.wikimedia.mediawiki.flink.connectors.http.table.HttpAsyncTableFunction;
import org.wikimedia.mediawiki.flink.connectors.http.table.HttpTableSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.flink.table.factories.utils.FactoryMocks.createTableSource;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class HttpTableSourceTest {
    private static final String SAMPLES_FOLDER = "/http/";
    private static final ResolvedSchema SCHEMA =
            new ResolvedSchema(
                    Arrays.asList(
                            Column.physical("revids", DataTypes.STRING().notNull()),
                            Column.physical("response", DataTypes.STRING().notNull())
                    ),
                    Collections.emptyList(),
                    UniqueConstraint.primaryKey("revids", Stream.of("revids").collect(Collectors.toList()))
            );
    private static WireMockServer wireMockServer;
    private final int[][] lookupKey = {{0}};

    @Test
    void testTableFunctionEval() {
        HttpTableSource table = (HttpTableSource) createTableSource(SCHEMA, getOptions());
        AsyncTableFunctionProvider<RowData> provider = (AsyncTableFunctionProvider<RowData>) table.getLookupRuntimeProvider(new LookupRuntimeProviderContext(lookupKey));
        HttpAsyncTableFunction tableFunction = (HttpAsyncTableFunction) provider.createAsyncTableFunction();
        assertNotNull(tableFunction);
    }

    private Map<String, String> getOptions() {
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("connector", "mediawiki-http");
        options.put("format", "json");
        options.put("url", "http://localhost:8088/api.php?action=query&format=json&prop=revisions&formatversion=2&rvprop=content&rvslots=main");
        return options;
    }
}
