package org.wikimedia.mediawiki.flink.connectors.http;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.formats.json.JsonFormatFactory;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.connector.source.DynamicTableSource;
import org.apache.flink.table.data.RowData;
import org.apache.flink.table.factories.DynamicTableFactory.Context;
import org.apache.flink.table.runtime.connector.source.LookupRuntimeProviderContext;
import org.apache.flink.table.types.DataType;
import org.eclipse.jetty.client.api.ContentResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.mediawiki.flink.connectors.http.util.HttpClientUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class HttpClientUtilsTest {
    private static final String SAMPLES_FOLDER = "/http/";

    private static WireMockServer wireMockServer;

    @Mock
    private Context dynamicTableFactoryContext;

    private DynamicTableSource.Context dynamicTableSourceContext;


    @BeforeAll
    public static void setUpAll() {
        wireMockServer = new WireMockServer();
        wireMockServer.start();
    }

    @AfterAll
    public static void cleanUpAll() {
        if (wireMockServer != null) {
            wireMockServer.stop();
        }
    }

    @BeforeEach
    public void setUp() {
        int[][] lookupKey = {{}};
        this.dynamicTableSourceContext = new LookupRuntimeProviderContext(lookupKey);

    }

    @Test
    public void shouldDeserializeActionApiResponse() throws IOException, ExecutionException, InterruptedException, TimeoutException {
        String baseUrl = wireMockServer.baseUrl();
        String queryString = "/api.php?action=query&format=json&prop=revisions&revids=1088481788&formatversion=2&rvprop=content&rvslots=main";
        wireMockServer.stubFor(
                get(urlEqualTo(queryString))
                        .withHeader("Content-Type", equalTo("application/json"))
                        .willReturn(
                                aResponse()
                                        .withStatus(200)
                                        .withBody(readFileAsBytes(SAMPLES_FOLDER + "ActionApiResponse.json"))
                        ));
        DataType physicalDataType = DataTypes.ROW(
                DataTypes.FIELD("batchcomplete", DataTypes.BOOLEAN()),
                DataTypes.FIELD("query", DataTypes.ROW(
                        DataTypes.FIELD("pages",
                                DataTypes.ARRAY(
                                        DataTypes.ROW(
                                                DataTypes.FIELD("title", DataTypes.STRING()),
                                                DataTypes.FIELD("revisions", DataTypes.ARRAY(
                                                        DataTypes.ROW(
                                                                DataTypes.FIELD("slots", DataTypes.ROW(
                                                                        DataTypes.FIELD("main", DataTypes.ROW(
                                                                                DataTypes.FIELD("content", DataTypes.STRING()))))))))))))
                ));

        DeserializationSchema<RowData> schemaDecoder = new JsonFormatFactory()
                .createDecodingFormat(dynamicTableFactoryContext, new Configuration())
                .createRuntimeDecoder(dynamicTableSourceContext, physicalDataType);


        ContentResponse request = HttpClientUtils.buildHttpClient(null, null)
                .newRequest(baseUrl + queryString).header("Content-Type", "application/json").send();
        String response = request.getContentAsString();
        byte[] data = response.getBytes(StandardCharsets.UTF_8);

        RowData deserialized = schemaDecoder.deserialize(data);
        String content = String.valueOf(deserialized
                .getRow(1, 0) // query
                .getArray(0) // first element
                .getRow(0, 0) // pages
                .getArray(1) // revisions
                .getRow(0, 0) // slots
                .getRow(0, 0) // main
                .getRow(0, 0) // content
                .getString(0));

        String expected = "[[Category:Transport ministers by country|Namibia]]\n[[Category:Government ministers of Namibia]]";
        assertEquals(expected, content);

    }

    private byte[] readFileAsBytes(String s) {
        byte[] data;
        try {
            data = Files.readAllBytes(Paths.get(String.valueOf(this.getClass().getResource(s).getFile())));

        } catch (IOException e) {
            data = null;
        }
        return data;
    }
}


