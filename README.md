[![Project Status: Concept <E2><80><93> Minimal or no implementation has been done yet, or the repository is only intended to be a limited example, demo, or proof-of-concept.](https://www.repostatus.org/badges/latest/concept.svg)](https://www.repostatus.org/#concept)

This repo contains a PoC flink connector that implements lookup join semantics.
It allows to send http async get requests to an endpoint and retrieve content.

It has been developed in the context of  [https://phabricator.wikimedia.org/T318856](https://phabricator.wikimedia.org/T318856).
For more info see [https://www.mediawiki.org/wiki/Platform_Engineering_Team/Event_Platform_Value_Stream/Build_simple_stateless_service_using_Flink_SQL](Build_simple_stateless_service_using_Flink_SQL).

A demo enrichment statelss application is provided in `flink-http-action-connector.sql`.

# Getting started
Build with
```bash
./mvnw package -Dcheckstyle.skip -Dforbiddenapis.skip
```

After having started a local flink cluster launch `flink-http-action-connector.sql`.
```
./bin/sql-client.sh  -j flink-mediawiki-http-connector/target/mediawiki-http-connector-1.0-SNAPSHOT-jar-with-dependencies.jar  -f flink-http-action-connector.sql
```