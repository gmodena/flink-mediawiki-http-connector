SET sql-client.execution.result-mode=TABLEAU;

-- This connector implements a LookupTableSource interface. It allows to send http requests
-- to an endpoint during a JOIN operation. An ActionAPI table using this connector must follow this semantics:
-- 1. domain field should be present (we need it to set the Host head field in a request).
-- 2. a response  content field should be present (we need it to store the API response as string).
-- all other schema fields will be used as parameters to the query string.
CREATE TABLE MwAction(
    revids integer, -- this field will be part of the query string (revids=<val>). It's name should match the action api param
    domain string, -- special field (not part of action api) that we need to set in the request header. Optional?
    response string -- special field (not part of action api) that will store the action api response content.
) WITH (
    'connector' = 'mediawiki-http', -- the connect identifier.
    'format' = 'json', -- TODO: we don't need really this, since internally we don't need to serialize data as Json.
    'url' = 'https://en.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&formatversion=2&rvprop=content&rvslots=main', -- the base url to query. Can contain defaults query string params. Query parameters could also be declared as table columns, and used in where clauses (... where rvslots = 'main');
    -- 'fields.domain' = 'domain', -- Optional field that stores the domain (default: `domain`). When present, it will
    -- be used to set the Host request header.
    'fields.response' = 'response', -- Field that stores the response payload (default: `response`).
    'http.client.header.user-agent' = 'wmf-mediawiki-stream-enrichment/1.0-SNAPSHOT bot'
);

-- A mock revision-create stream.
-- This should contain `domain`, but datagen does not let us mock arbitrary
-- valutes (only random / sequences of numeric values)
CREATE TABLE MockRevisionCreate(
        rev_id integer,
        page_id STRING,
        proc_time AS PROCTIME() -- The http connector requires Temporal join semantics.
) WITH (
        'connector' = 'datagen',
        'rows-per-second' = '1',
        'fields.rev_id.kind' = 'sequence',
        'fields.rev_id.start' = '1000',
        'fields.rev_id.end' = '1100', -- stream 100 revisions
        'fields.page_id.kind' = 'sequence',
        'fields.page_id.start' = '1000',
        'fields.page_id.end' = '2000'
);


-- A mock enriched-revision-create.
-- This should contain `domain`, but datagen does not let us mock arbitrary
-- values (only random / sequences of numeric values)
CREATE TABLE MockRevisionCreateEnriched (
    rev_id integer,
    domain string,
    wikitext string
) WITH (
      'connector' = 'print');

-- INSERT INTO MockRevisionCreateEnriched -- This INSERT would produce into a topic. `print is only displayed in Flink's cluster UI.
SELECT
    mw_action.revids,
    mw_action.domain,
    JSON_VALUE(mw_action.response, '$.query.pages[0].revisions[0].slots.main.content') as wikitext -- parse the response payload as json an extract the wikitext field.
FROM MockRevisionCreate AS revision_create JOIN MwAction FOR SYSTEM_TIME AS OF revision_create.proc_time AS mw_action
ON revision_create.rev_id = mw_action.revids
where mw_action.domain = 'en.wikipedia.org'; -- this should be a field we join on, extracted from `meta`.
